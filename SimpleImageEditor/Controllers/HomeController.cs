﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleImageEditor.Tools;

namespace SimpleImageEditor.Controllers
{
    public class Editor
    {
        public string Id { get; set; }
        public string Image { get; set; }
        public bool Type { get; set; }
        
    }
    public class HomeController : Controller
    {

        
        public ActionResult Index()
        {
            var image = new Editor() { Id = "GUID1502281991", Image = "ussr.jpg" }; 
            return View(image);
        }
         [HttpPost]
           public JsonResult Handler(string ID,float imageH, float imageW,float imageRotate,string imageSource,float imageX,float imageY,
           float selectorH,float selectorW,float selectorX,float selectorY,bool isContainer)
       {

             var arg=new HandlerParametres(){ID = ID,imageH = imageH,imageW = imageW,imageRotate = imageRotate,imageSource = imageSource,imageX = imageX,imageY = imageY,
             selectorH = selectorH,selectorW = selectorW,selectorX = selectorX,selectorY = selectorY,IsContainer = isContainer};
       
             var t=ImageHandler.ImageProcessing(arg,Server.MapPath("~"));
           return Json(t);
           }

    
    }
  
    
}
