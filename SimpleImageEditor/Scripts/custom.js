﻿/***************************************************************
 *  Using:
 *  FC-CropResizer
 *  imageAreaSelect
 *   
 *  Dependencies:
 *  Bootstrap 
 *  Bootstrap-slider
 *  JQueryRotate
 *  JQuery
 *
 *  Copyright (C) 2013 by Leonid Kulinchik 
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***************************************************************/
var SimpleImageEditor = { // NAMESPACE
    SetCrop: function ($img, params) {
        console.log('base set Crop');
        SimpleImageEditor.archive[$img].SetCrop(params);
    },
    archive : {},
    getObject : function(id) {
        if (typeof this.archive[id] == "undefined") {
            this.archive[id] = new this.fc(id);
        }
        return this.archive[id];
    },
    Send:function () {
        console.log('send all');
        for (var t in this.archive)
            this.archive[t].send();
    }
};

SimpleImageEditor.fc = function (id) { // Constructor
    // Vars
    console.log('SimpleImageEditor Init');
    this.id = id;
    // Image data
    this.source;//name of image
    this.Width = 0;
    this.Height = 0;
    this.MaxWidth = 1200;
    this.MaxHeight = 800;
    this.MinWidth = 50;
    this.MinHeight = 50;
    //offset
    this.Top = 0;
    this.Left = 0;
    //container
    this.Container;
    this.cTop;
    this.cLeft;
    this.cWidth;
    this.cHeight;
    //rate of image
    this.Rate = 0;
    // flags
    this.isCrop=true;
    this.isRotate = true;
    this.isResize = true;
    this.isZoom = true;
    // images
    this.img;
    this.preview;
    //rotate angle
    this.angle = 0;
    // zoom
    this.zoom;
    // aspect ratio (example '3:2')
    this.aspectRatio;
    //function on croop changing
    this.onCroopChange;
    // slider for rotate
    this.rotateSlider;
    //slider for zoom
    this.zoomSlider;
    //position of image
    this.posX = 50;
    this.posY = 50;
    // Move
    this.X0 ;
    this.Y0 ;
    // Resize div data
    this.resizeWidth = 16;
    this.resizeHeight = 16;
    this.resizeTop = 0;
    this.resizeLeft = 0;
    // Resize move
    this.BuferWidth = 0;
    this.BuferHeight = 0;
    //Crop
    this.isSelectContainer=false;
    this.cropTop;
    this.cropLeft;
    this.cropHeight;
    this.cropWidth;
    this.cropAngle;
    //selection container

    this.sTop;
    this.sLeft;
    this.sWidth;
    this.sHeight;
    // Flags
    
    this.resizeMoveState = false;
    this.selectionState = false;
    this.saveProportions = function(evt) { return evt.shiftKey; };
    this.withContainer = true;
    this.stopSelection = true;
   
    this.debugMode = true;
    // Handlers
    this.onUpdate = function () {
    };
    // Nodes
    this.image = null;
 
    this.resize = null;
};
SimpleImageEditor.fc.prototype = {
    // Const
    ERRORS: {
        1: "Object initialisation error",
        2: "Image was not found",
        3: "Element in not image",
        4: "Callback-function [onUpdate] was not defined",
        5: "HTML-element BODY was not defined"
    },
    // Methods
    // Default
    gebi: function(id) {

        var tmp = document.getElementById(id);

        return tmp;
        return $('#' + id);
    },
    rotate: function(slider) {
        this.angle = slider.value;
        if (this.img != null) {
            this.img.rotate(this.angle);
        }
        //this.log(this.angle);

        if (this.preview != null) {
            this.preview.rotate(this.angle);
        }
        this.setResizeVars();
    },
    //addContainer:function() {
        
    //},
    addRotateHandler: function(slider) {
        var that = this;
        this.rotateSlider = $(slider);
        console.log('init slider');

        this.rotateSlider.slider();

        this.rotateSlider.on('slide', function(ev) { that.rotate(ev); });

    },  
        
    addHandler: function(object, event, handler, useCapture) {
        if (object.addEventListener) {
            object.addEventListener(event, handler, useCapture ? useCapture : false);
        } else if (object.attachEvent) {
            object.attachEvent('on' + event, handler);
        } else alert(this.errorArray[9]);
    },
    defPosition: function(event) {
        var x = y = 0;
        if (document.attachEvent != null) {
            x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
            y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
        }
        if (!document.attachEvent && document.addEventListener) { // Gecko 
            x = event.clientX + window.scrollX;
            y = event.clientY + window.scrollY;
        }
        return { x: x, y: y };
    },
    absPosition: function(obj) {
        var x = y = 0;
        while (obj) {
            x += obj.offsetLeft;
            y += obj.offsetTop;
            obj = obj.offsetParent;
        }
        return { x: x, y: y };
    },
    domReady: function(i) { /* Copyright http://ajaxian.com/ */
        var u = navigator.userAgent;
        var e =/*@cc_on!@*/ false;
        var st = setTimeout;
        if (/webkit/i.test(u)) {
            st(
                function() {
                    var dr = document.readyState;
                    if (dr == "loaded" || dr == "complete") i();
                    else st(arguments.callee, 10);
                },
                10
            );
        } else if ((/mozilla/i.test(u) && !/(compati)/.test(u)) || (/opera/i.test(u))) {
            document.addEventListener("DOMContentLoaded", i, false);
        } else if (e) {
            (
                function() {
                    var t = document.createElement('doc:rdy');
                    try {
                        t.doScroll('left');
                        i();
                        t = null;
                    } catch(e) {
                        st(arguments.callee, 0);
                    }
                })();
        } else window.onload = i;

    },
    // Common
    //log: function (keys) {
    //    if (!this.debugMode) return;
    //    console.log('angle= '
    //      + this.angle);
    //},
    debug: function(keys) {
        if (!this.debugMode) return;
        var mes = "";
        for (var i = 0; i < keys.length; i++) mes += this.ERRORS[keys[i]] + " : ";
        mes = mes.substring(0, mes.length - 3);
        alert(mes);
    },
    send: function() {
        var _self = this;
        var response = this.getParameters();
        console.log('top :', this.Top);
        console.dir(response);
        $.ajax({
            url: '/Home/Handler',
            type: 'Post',
            data: response,
            success: function(r) {
                _self.data('imageResult', r);

            }
        });
    },
    getParameters: function() {
        //var image = _self.data('image');
        //var selector = _self.data('selector');

        var fixed_data = {
            //'viewPortW': _self.width(),
            //'viewPortH': _self.height(),
            'ID':this.id,
            'imageX': this.Left,
            'imageY': this.Top,
            'imageRotate': this.angle,
            'imageW': this.Width,
            'imageH': this.Height,
            'imageSource': this.source,
            'selectorX': this.cropLeft,
            'selectorY': this.cropTop,
            'selectorW': this.cropWidth,
            'selectorH': this.cropHeight,
            'isContainer':this.isSelectContainer
        };
        console.dir(fixed_data);
        return fixed_data;
    },
    imageArea: function() {
        console.log('image area');
        var x, y, h, w;
        x = this.Left;
        y = this.Top;
        h = this.Height;
        w = this.Width;
        console.log(x, y, w, h);
        return { x: x, y: y, w: w, h: h };
    },
    init: function(hash, node) {

        if (typeof node == "undefined") {
            var _this = this;
            this.domReady(
                function() { _this.init(hash, 1); }
            );
            return;
        }

        this.img = $('img#' + this.id);
        this.image = this.img[0];

        if (this.image == null) {
            this.debug([1, 2]);
            return;
        }
        if (this.image.nodeName.toLowerCase() != "img") {
            this.debug([1, 3]);
            return;
        }

        //try {
        console.log('resizer init');
        for (var i in hash) this[i] = hash[i];
        if (this.onUpdate == null) {
            this.debug([1, 4]);
            return;
        }
        $('#sumbit').on('click', function(e) {

            console.dir(this);
            //console.dir(SimpleImageEditor.archive[this.id]);
            SimpleImageEditor.Send();
            //e.preventDefault();

        });

        if (!document.body) {
            this.debug([1, 5]);
            return;
        }
        this.image.ondragstart = function() { return false; }; // IE fix
        // Set default
        this.redefine();

        this.Rate = this.Width / this.Height;

        this.MaxWidth = getPageSize()[5]; //Width of display
        this.MaxHeight = getPageSize()[4] - 100;

        if (this.img == null) {
            this.img = $(this.id);
            ;
        }
        
        if (this.withContainer) {
            this.addContainer();
            this.setSelectContainer();
        }
        this.checkImage();
        
        if (this.isCrop && this.isSelectContainer) {
            console.log('init crop');
            $(this.selectContainer).imgAreaSelect({
                aspectRatio: this.aspectRatio,
                //handles: false,
                id:this.image.id,
                container: {
                    left: this.cLeft, top: this.cTop,
                height:this.cHeight,width:this.cWidth},
                // parent:$('#worksurface'),
                //x0:10,y0:10,x1:200,y1:200,
                onSelectEnd: SimpleImageEditor.SetCrop,
                
            });
        } else if (this.isCrop && this.img != null) {
            console.log('init crop');
            $(this.img).imgAreaSelect({
                //aspectRatio: this.aspectRatio,
                //handles: false,
                container: this.Container,
                // parent:$('#worksurface'),
                //x0:10,y0:10,x1:200,y1:200,
                onSelectEnd: SimpleImageEditor.SetCrop,
               
            });
        }
        ;
        console.log('Top :', this.Top);
        this.drawResizeBlock();
        // Add handers
        var _this = this;
        this.addHandler(document, "mousemove", function(evt) {

            _this.resizeMoveHandler(evt);
        });
        this.addHandler(document, "mouseup", function() {

            _this.resizeMoveState = false;
            _this.redefine();
        });

        this.addHandler(_this.resize, "mousedown", function(evt) {
            _this.resizeMoveState = true;
            evt = evt || window.event;
            if (evt.preventDefault && _this.stopSelection)
                evt.preventDefault();
            _this.X0 = _this.resizeLeft;
            _this.Y0 = _this.resizeTop;
            _this.BuferWidth = _this.Width;
            _this.BuferHeight = _this.Height;
        });

        if (this.rotateSlider != undefined) {

            this.addRotateHandler(this.rotateSlider);
        }


        console.log(this.cTop, this.cLeft, this.cHeight, this.cWidth);

    


//} catch (e) {
        //    console.log('error catched:'+e);
            
        //    this.debug([1]);
        //}
        console.log('Top :', this.Top);

    },
    addContainer: function () {
        var div =$('#worksurface')[0];
        if (div == null || div.firstChild == this.image) {
            console.log('container not found');
            //div = document.createElement("div");
            //div.appendChild(this.image.cloneNode(true));
            //this.image.parentNode.replaceChild(div, this.image);
            //this.image = div.firstChild;
            return;
        }
        this.Container = div;
        var a=$(window).height();   // returns height of browser viewport
        var b=$(document).height(); // returns height of HTML document
        var c=$(window).width();   // returns width of browser viewport
        var d = $(document).width(); // returns width of HTML document
        console.log('sizes', a, b, c, d);
        var Cx = c - this.absPosition(div).x-10;
        var Cy = a-this.absPosition(div).y-10;
        if ((Cx / 3) > Cy) Cx = Cy*2;
        div.style.width = Cx+ "px";
        div.style.height = Cy + "px";
       
      
        //div.style.paddingTop =  this.posY + "px";
        //div.style.paddingLeft = this.posX + "px";
        this.MaxHeight = Math.sqrt(Cy*Cy/(1+this.Rate*this.Rate));
        this.MaxWidth = this.MaxHeight * this.Rate;
        this.posY = (Cy-this.MaxHeight)/2;
        this.posX = (Cx-this.MaxWidth)/2;

        this.cLeft = this.absPosition(div).x;
        this.cTop = this.absPosition(div).y;
        this.cWidth = Cx;
        this.cHeight = Cy;
        div.style.overflow = 'hidden';
        div.className = 'well';
       
       
    },
    redefine: function() {
        this.Top = this.absPosition(this.image).y;
        this.Left =  this.absPosition(this.image).x;
       
        this.Width = this.image.style.width ? parseInt(this.image.style.width) : this.image.offsetWidth;
        this.Height = this.image.style.height ? parseInt(this.image.style.height) : this.image.offsetHeight;
        
        var W, H, Y = this.Height, X = this.Width, angle = this.angle;
        //offset
        W = X / 2 + this.Left;
        H = Y / 2 + this.Top;
        //rotate
        if ((angle >= 0) && (angle < 90)) { }
        else if ((angle >= 90) && (angle < 180)) {
            Y = -Y;
        }
        else if ((angle >= 180) && (angle < 270)) {
            X = -X;
            Y = -Y;
        }
        else if ((angle >= 270) && (angle < 360)) {
            X = -X;
        }
        Dx = (X * Math.cos(toRadians(angle)) - Y * Math.sin(toRadians(angle))) / 2;
        Dy = (X * Math.sin(toRadians(angle)) + Y * Math.cos(toRadians(angle))) / 2;
        W += Dx;
        H += Dy;
        this.resizeTop = H;
        this.resizeLeft = W;
       
        //this.imageArea();
        this.onUpdate();
    },
    // Resize
    drawResizeBlock: function() {
        if (!this.gebi("resizeDivId_" + this.id)) {
            this.resize = document.createElement("div");
            this.resize.id = "resizeDivId_" + this.id;
            this.resize.className = "resizeDiv";
            //this.resize.innerHTML = "<img src=\"i/0.gif\" width=\"1\" height=\"1\" alt=\"\">";
            this.resize.style.width = this.resizeWidth + "px";
            this.resize.style.height = this.resizeHeight + "px";
            this.resize.style.display = "none";
            document.body.appendChild(this.resize);
        }
        this.resize = this.gebi("resizeDivId_" + this.id);
        this.redefine();
        this.setResizeVars();
        this.resize.style.display = "";
    },
    setResizeVars: function() {
        this.redefine();
        console.log('setresizewars');
        console.dir(this);
        this.resize.style.top = this.resizeTop - 10 + "px";
        this.resize.style.left = this.resizeLeft - 10 + "px";
        this.updateSelectContainer();

    },
    checkImage: function () {
        var flag = false;
        this.Top = this.posY + this.cTop;
        this.Left = this.posX + this.cLeft;
        this.image.style.position = 'relative';
        this.image.style.left = this.posX + 'px';
        this.image.style.top = this.posY + 'px';
        if (this.Width <= this.MinWidth) {
            this.Width = this.MinWidth;
            flag = true;
        }
        if (this.Width > this.MaxWidth) {
            this.Width = this.MaxWidth;
            flag = true;
        }
        if (this.Height <= this.MinHeight) {
            this.Height = this.MinHeight;
            flag = true;
        }
        if (this.Height > this.MaxHeight) {
            this.Height = this.MaxHeight;
            flag = true;
        }
        if (flag){
        this.image.style.width = this.Width + "px";
        this.image.style.height = this.Height + "px";
        }
    },
    SetCrop: function (selection) {
        console.log('set crop');
        this.cropWidth = selection.width;
        this.cropHeight = selection.height;
        this.cropTop = selection.y1;
        this.cropLeft = selection.x1;
        this.cropAngle = selection.angle;
       
            console.log(selection);
            console.dir(this);
       
    },
    resizeMoveHandler: function (evt) {
        
        if (!this.resizeMoveState) return;
        evt = evt || window.event;
        var hW = 0, hH = 0, offset = function () { x: 0; y: 0 }, aoffset = function () { x: 0; y: 0 };
        var X = offset.x = this.defPosition(evt).x - this.X0;
        var Y = offset.y = this.defPosition(evt).y - this.Y0;
        var angle = this.angle;
        if ((angle >= 0) && (angle < 90)) { }
        else if ((angle >= 90) && (angle < 180)) {
            Y = -Y;
        }
        else if ((angle >= 180) && (angle < 270)) {
            X = -X;
            Y = -Y;
        }
        else if ((angle >= 270) && (angle < 360)) {
            X = -X;
        }
        aoffset.x =  X * Math.cos(toRadians(this.angle)) - Y * Math.sin(toRadians(this.angle));
        aoffset.y =  X * Math.sin(toRadians(this.angle)) + Y * Math.cos(toRadians(this.angle));
        hW = this.BuferWidth + aoffset.x;
        hH = this.BuferHeight + aoffset.y;

        if (this.debugMode) {
            console.log(this.X0, this.Y0);
        }
        if (this.saveProportions(evt)) {
        if (this.Rate < 1) {
             hH = hW / this.Rate;
        } else {
             hW = this.Rate * hH;
        }}
        if (hW <= this.MinWidth)  hW = this.MinWidth;
        if (hW >  this.MaxWidth)  hW = this.MaxWidth;
        if (hH <= this.MinHeight) hH = this.MinHeight;
        if (hH >  this.MaxHeight) hH = this.MaxHeight;
        this.image.style.width = hW + "px";
        this.image.style.height = hH + "px";
        if (this.debugMode) {
            console.log(
                'angle ' + this.angle + '\t\tX \tY ' +
                    '\n|prew\t\t' + this.BuferHeight + '\t' + this.BuferWidth +
                    '\n|unknown\t' + this.X0 + '\t' + this.Y0 +
                    '\n|offset\t\t' + offset.x + '\t' + offset.y +
                    '\n|angofst\t' + aoffset.x + '\t' + aoffset.y +
                    '\n|result\t\t' + hW + '\t' + hH
            );
        }
        this.setResizeVars();
    },
    setSelectContainer: function  () {
        console.log('create select container');
            var div = document.createElement("div");

       
            div.style.position = 'absolute';
            //div.style.opacity = 0.1;
            //div.style.filter = 'alpha(opacity=' + 1 + ')';
            //div.style.background = '#ffff00';
            div.style.border = "thin dashed  #0000FF";
div.style.overflow = 'hidden';
div.setAttribute("id", "SelectContainer");
        
       
this.selectContainer = div;
        
this.updateSelectContainer();
this.Container.appendChild(this.selectContainer);
this.isSelectContainer = true;

    },
    updateSelectContainer: function () {
        console.log('update select container');
        if (this.selectContainer!==undefined){
            var width = abs(this.Width * cos(toRadians(this.angle))) + abs(this.Height * sin(toRadians(this.angle)));
            var height = abs(this.Height * cos(toRadians(this.angle))) + abs(this.Width * sin(toRadians(this.angle)));
         console.log('imgH:' + height + ' imgW:' + width);

         var top = this.posY +20;
         var left = this.posX +35;
         top = top - (height - this.Height) / 2;
         left = left - (width - this.Width) / 2 ;
         this.sTop = top;
         this.sLeft = left;
         this.sHeight = height;
            this.sWidth = width;
         $(this.selectContainer).css({
             left: left, top: top,
             width: width, height: height
         });
         //this.selectContainer.style.width = width + "px";
         //this.selectContainer.style.height = height + "px";

         //this.selectContainer.style.paddingTop = this.posY + "px";
         //this.selectContainer.style.paddingLeft = this.posX + "px";
        }
     },
};
var abs = Math.abs,
    max = Math.max,
    min = Math.min,
    round = Math.round,
    sin=Math.sin,
    cos=Math.cos;

/**
 * Create a new HTML div element
 * 
 * @return A jQuery object representing the new element
 */
function div() {
    return $('<div/>');
}
function preview(img, selection) {
    var scaleX = 100 / (selection.width || 1);
    var scaleY = 100 / (selection.height || 1);
    var image = $('#preview_image');
    //var angle = $('#Rotate').value;

    //image.rotate(angle);
    image.css({
        width: Math.round(scaleX * 400) + 'px',
        height: Math.round(scaleY * 300) + 'px',
        marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
        marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
    });
    
}




function toRadians(angle) {
    return angle * (Math.PI / 180);
}
function getPageSize() {
    var xScroll, yScroll;
    var scr_w = screen.width;
    var scr_h = screen.height;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight) { // Explorer 6 strict mode
        xScroll = document.documentElement.scrollWidth;
        yScroll = document.documentElement.scrollHeight;
    } else { // Explorer Mac...would also work in Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) { // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if (yScroll < windowHeight) {
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    return [pageWidth, pageHeight, windowWidth, windowHeight,scr_h,scr_w];
}
