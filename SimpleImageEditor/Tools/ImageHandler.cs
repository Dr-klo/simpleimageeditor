﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace SimpleImageEditor.Tools
{
      public class HandlerParametres
    {
       public string ID;
       public float imageH; //Высота изображения (используются для зумирования)
       public float imageW;//Ширина изображения
       public float imageRotate;//угол поворота
       public string imageSource;//имя рисунка
       public float imageX;// смещение изображения
       public float imageY;
       public float selectorH; //кроп
       public float selectorW;
       public float selectorX;
       public float selectorY;
       public bool IsContainer;
    }

    public static class ImageHandler
    {

        public static string ImageProcessing(HandlerParametres arg, string servPath)
        {

            string id = arg.ID;// DateTime.Now.Ticks.ToString(); //is unique identifier
            float _width = arg.imageW; // parametres of displayed image
            float _height = arg.imageH;
            servPath += "Content/";

            Bitmap img = (Bitmap)Bitmap.FromFile(servPath+'/'+arg.imageSource);

            img.Save(servPath + '/' + String.Format("test{0}_original.jpg", id)); //Original image
            //Resize
            Bitmap image_p = ResizeImage(img, _width, _height);

            int width = image_p.Width; //real parametres of image
            int height = image_p.Height;
            image_p.Save(servPath + '/' + String.Format("test{0}_resized.jpg", id)); //Resized image
            //Rotate if angle is between 0.00 and 360
            if (arg.imageRotate > 0.0F && arg.imageRotate < 360.00F)
            {
                image_p = (Bitmap)RotateImage(image_p, arg.imageRotate);
                width = image_p.Width;
                height = image_p.Height;
            }


            var scale = (float)(width / (Math.Abs(arg.imageW * Math.Cos(Deg2Rad(arg.imageRotate))) +
                Math.Abs(arg.imageH * Math.Sin(Deg2Rad(arg.imageRotate))))); //set scale for zooming crop
          
            image_p.Save(servPath + '/' + String.Format("test{0}_rotated.jpg", id));//Rotated image
            //Get Selector Portion
            image_p = ImageCopy(image_p, 0, 0, arg.selectorX, arg.selectorY, arg.selectorW, arg.selectorH,scale);
          
            image_p.Save(servPath + '/' + String.Format("test{0}_croped.jpg",id));//Croped image


            image_p.Dispose();
            img.Dispose();
           ;
            return String.Format("test{0}_croped.jpg", id);
        }
        private static Bitmap ImageCopy(Bitmap srcBitmap, float src_x, float src_y, float dst_x, float dst_y,
            float dst_width, float dst_height,float scale)
        {
            // Create the new bitmap and associated graphics object
            if (scale > 0) //zooming crop parametres
            {
                dst_x *= scale;
                dst_y *= scale;
                dst_height *= scale;
                dst_width *= scale;
            }

           
            RectangleF sourceRec = new RectangleF(src_x, src_y, dst_width, dst_height);
            RectangleF cropRect = new RectangleF(dst_x, dst_y, dst_width, dst_height);
            Bitmap bmp = new Bitmap(Convert.ToInt32(dst_width), Convert.ToInt32(dst_height));
            using(Graphics g = Graphics.FromImage(bmp))
            {            // Draw the specified section of the source bitmap to the new one
            g.DrawImage(srcBitmap, sourceRec, cropRect, GraphicsUnit.Pixel);
            // Clean up
            }

            // Return the bitmap
            return bmp;

        }
      
        private static Bitmap ResizeImage(Bitmap img, float width, float height)
        {
            int _width = img.Width;
            int _height = img.Height;
            var _rate = (float)_width / _height;//Custom rate
            float rate = (float)width / height;//Real rate
            float h;
            float w;
            if (_rate < rate) //set new rate for our image (is 'resizing')
            {
                h = (float)_height;
                w = (rate / _rate) * _width;

            }
            else
            {
                h = (float)(_rate / rate) * _height;
                w = _width;
            }
            var newImg = new Bitmap((int) w, (int)h);
            Graphics g = Graphics.FromImage(newImg);
            g.DrawImage(img, new Rectangle(0, 0, (int)w,(int) h));
            
            g.Dispose();
            return newImg;
           
        }
        private static double Deg2Rad(double angle)
        {
           return angle * (Math.PI / 180);
        }
        private static Image RotateImage(Bitmap img, double rotationAngle)
        {
            // calculate the size of the new canvas
            int h=Math.Abs((int) (Math.Abs(img.Height*Math.Cos(Deg2Rad(rotationAngle)))+Math.Abs(img.Width*Math.Sin(Deg2Rad(rotationAngle)))))
                , w = Math.Abs((int)(Math.Abs(img.Width * Math.Cos(Deg2Rad(rotationAngle))) + Math.Abs(img.Height * Math.Sin(Deg2Rad(rotationAngle)))));
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(w, h);
           
            int offsetY  = (h - img.Height)/2,
                offsetX = (w - img.Width) / 2;
            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);
            
            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform((float)rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(offsetX, offsetY));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }

    }

}